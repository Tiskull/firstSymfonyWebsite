<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Form\ArticleType;
use App\Form\CommentType;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class BlogController extends Controller
{
    /**
     * Configuration de la route
     * @Route("/blog", name="blog")
     * Injection de dependances : Symfony cherche un ArticleRepository utilisable
     */
    public function index(ArticleRepository $repo)
    {
        
        /** Cette ligne est inutile si l'on utilise l'injection de dépendances 
        */
        //$repo = $this->getDoctrine()->getRepository(Article::class);

        /**
         * Charge tout les articles du repo
         * Pour rappel, lorsque j'utilise l'orm doctrine le repository gère les select de données
         */
        $articles = $repo->findAll();

        /** Transmet à la vue le controller et la collection d'articles */
        return $this->render('blog/index.html.twig', [
            'controller_name' => 'BlogController', 'articles' => $articles
        ]);
    }

    /**
     * @Route("/", name="home")
     */
    public function home(){
        return $this->render('blog/home.html.twig', [
            'title' => "Bienvenue ici les amis",
            'age' => 31
        ]);
    }
    
    /**
     * Cette fonctions sera appellé lors de la création et la modification
     * @Route("/blog/new",name="blog_create")
     * @Route("/blog/{id}/edit", name="blog_edit")
     */
    public function form(Article $article = null, Request $request, ObjectManager $manager){

        if(!$article){
            $article = new Article();
        }

        /**Creation d'un formulaire via la classe ArticleType*/
        $form = $this->createForm(ArticleType::class, $article);

        /**Autre façon de faire moins propre */
        // $form = $this->createFormBuilder($article)               
        // ->add('title')
        // ->add('content')
        // ->add('image')
        // ->getForm();

        // $form = $this->createFormBuilder
        // ->add('title', TextType::class, [
        //     'attr' => [
        //         'placeholder' => "Titre de l'article"
        //     ]
        // ])

        /**Traitement du formulaire et ajout des champs dans $article. Symfony fait le lien entre le formulaire et l'article apparemment*/
        $form->handleRequest($request);

        /**Verification formulaire */
        if($form->isSubmitted() && $form->isValid()){
            if(!$article->getId()){
                $article->setCreatedAt(new \DateTime());
            }

            /** Le manager persiste l'article dans la bdd */
            $manager->persist($article);
            $manager->flush();

            /** Redirection sur la page qui décris l'article passé en paramètre */
            return $this->redirectToRoute('blog_show', ['id' => $article->getId()]);
        }

        /** Transmet à la vue une vue qui correspond au formulaire*/
        return $this->render('blog/create.html.twig', [
            'formArticle' => $form->createView(),
            'editMode' => $article->getId() !== null
        ]);
    }

    /**
     * @Route("/blog/{id}", name="blog_show")
     */
    public function show(Article $article, Request $request, ObjectManager $manager){
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $comment->setCreatedAt(new \DateTime())
            ->setArticle($article);
            $manager->persist($comment);
            $manager->flush();

            return $this->redirectToRoute('blog_show', ['id' => $article->getId()]);
        }

        return $this->render('blog/show.html.twig', [
            'article' => $article,
            'commentForm' => $form->createView()
        ]);
    }
}
